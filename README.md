# OOP Lab12

This is an application for downloading a URL to a file.

1. Fork (not clone) this repository: https://bitbucket.org/skeoop/downloader  
2. Run it.  If you download a large file, the user interface freezes.  
3. Modify the `DownloadApp` class to run the UI in the Swing Event Dispatcher thread.  
4. Rewrite the `URLReader` class to be a subclass of `SwingWorker` so you can `execute()` it from the DownloaderUI without hanging.  **Hint:** the `readUrl()` method should become (or be called from) the SwingWorker's `doInBackground()` method.
5. Modify `DownloaderUI` class. In the DownloadAction, use your `SwingWorker` to download the file so the UI does not hang or freeze.  
6. Add `publish` commands to the `doInBackground` method so that it "publishes" each time 1024 bytes is read.  Add code to the DownloaderUI to show progress of how many bytes have been downloaded. You can use a field, a JProgressBar (maybe the easiest), or other solution.
7. Demonstrate that you can now download a large file without the UI freezing.
8. Save your work on Bitbucket as `Lab12`.